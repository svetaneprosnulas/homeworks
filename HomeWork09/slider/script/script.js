//  Создать HTML разметку, содержащую кнопки `Previous`, `Next`, и картинки (6 штук), которые будут пролистываться горизонтально. 
//  На странице должна быть видна только одна картинка. Слева от нее будет кнопка `Previous`, справа - `Next`.
//  По нажатию на кнопку `Next` - должна появиться новая картинка, следующая из списка.
//  По нажатию на кнопку `Previous` - должна появиться предыдущая картинка.
//  Слайдер должен быть бесконечным, т.е. если в начале нажать на кнопку `Previous`, то покажется последняя картинка, а если нажать на `Next`, когда видимая - последняя картинка, то будет видна первая картинка.
//  Пример работы слайдера можно увидеть(http://kenwheeler.github.io/slick/) (первый пример).


window.addEventListener("DOMContentLoaded", function() {
    // достаем элементы с html(кнопки, слайды)
    let viewport = document.getElementById("viewport").offsetWidth;
    let btnNext = document.getElementById("next");
    let btnPrev = document.getElementById("prev");
    let slider = document.querySelector("div.slider");
    let viewSliders = document.querySelectorAll(".viewSlide");
    // переменная для индикатора слайда
    let viewSlide = 0;
    viewSliders[0].style.backgroundColor = "black";

    // Обработка клика на кнопку вперёд
    btnNext.addEventListener("click", function() {
        // Делаем индикатор слайда серым
        viewSliders[viewSlide].style.backgroundColor = "gray";
        // Условие, если номер слайда меньше 5
        if (viewSlide < 5) { // Если верно то
            // Увеличиваем номер слайда на один
            viewSlide++;
        } else { // Иначе
            // Номер слайда равен нулю
            viewSlide = 0;
        }
        // Закрашиваем индикатор слайда в черный
        viewSliders[viewSlide].style.backgroundColor = "black";
        // Меняем позицию всего слайда
        slider.style.left = -viewSlide * viewport + "px";
    });


    // Обработка клика на кнопку назад
    btnPrev.addEventListener("click", function() {
        // Делаем индикатор слайда серым
        viewSliders[viewSlide].style.backgroundColor = "gray";
        // Условие, если номер слайда больше нуля
        if (viewSlide > 0) { // Если верно то
            // Уменьшаем номер слайда
            viewSlide--;
        } else { // Иначе номер слайда равен 5
            viewSlide = 5;
        }
        // Закрашиваем индикатор слайда в черный
        viewSliders[viewSlide].style.backgroundColor = "black";
        // Меняем позицию всего слайда
        slider.style.left = -viewSlide * viewport + "px";
    });
});