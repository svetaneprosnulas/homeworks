/* 
1

Реализовать функцию для создания объекта "пользователь". Написать функцию createNewUser(),
которая будет создавать и возвращать объект newUser. При вызове функция должна спросить
у вызывающего имя и фамилию. Используя данные, введенные пользователем, создать объект
newUser со свойствами firstName и lastName. Добавить в объект newUser метод getLogin(),
который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя,
все в нижнем регистре (например, Ivan Kravchenko → ikravchenko). Создать пользователя с 
помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
Вывести в консоль результат выполнения функции.

2
Возьмите выполненное задание выше(функция CreateNewUser()) и дополните его следующим функционалом:
При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) 
и сохранить ее в поле birthday.
Создать метод getAge, который будет возвращать сколько полтзователю лет.
Создать метод getPassword() который будет возвращать такой результат (Ikravchenko1992).
Вывести в консоль результаты работы функции createNewUser(), а так же функций getAge() и getPassword() созданного обьекта
*/

// class+
// вызов класса+
// передать в класс имя и фамилию+
// добавить в класс метод getLogin()+
// реализовать функциональность getLogin()-
// Вывод данных
// Из метода getLogin() вывести в консоль контекст

window.onload = () => {
    class CreateNewUser {
        constructor(name, lastname, birthday) {
            this.name = name;
            this.lastname = lastname;
            this.birthday = birthday;
        }
        getLogin() {
            console.log(this.name[0].toLowerCase() + this.lastname.toLowerCase()); //1 буква имени + фамилия по 1 заданию
        };
        getAge() {
            var numEl = parseInt(this.birthday.match(/\d\d\d\d/))
            var num = new Date().getFullYear();
            console.log(num - numEl); //наш возраст считаем текущий
        };
        getPassword() {
            var numEl = parseInt(this.birthday.match(/\d\d\d\d/))
            console.log(this.name[0].toUpperCase() + this.lastname.toLowerCase() + numEl); //наш пассворд
        };
    }

    const newUser = new CreateNewUser(prompt('Введите имя'), prompt('Введите фамилию'), prompt('Введите дату вашего рождения в формате dd/mm/yyyy'));

    console.log(newUser.getLogin(), newUser.getAge(), newUser.getPassword());

}
