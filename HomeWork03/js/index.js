var musicStyles = ["Джаз", "Блюз"]; //создаем начальный масив
document.write("Начальный массив musicStyles" + "<br>"  + musicStyles + "<br><br>");

musicStyles.push("Рок-н-ролл"); //добавляем элемент "Рок-н-ролл" в конец с помощью метода .push
document.write("Массив musicStyles с добавлением элемента в конец" + "<br>"  + musicStyles + "<br><br>");

musicStyles[1] = "Классика"; //меняем среднее значение элемента "Блюз"(у которого индекс 1) на значение "Классика"
document.write("Массив musicStyles с измененным средним значением элемента" + "<br>"  + musicStyles + "<br><br>");

let firstMusicStyle = musicStyles.shift(); //создаем новую переменную, которая будет показывать удаленный с musicStyles элемент
document.write("Первый удаленный элемент массива musicStyles" + "<br>" + firstMusicStyle + "<br><br>");

musicStyles.unshift ("Рэп", "Рэгги"); //вставляем в начало массива firstMusicStyles 2 новых элемента
document.write("Массив musicStyles с добавлением новых элементов в начало" + "<br>" + musicStyles);
