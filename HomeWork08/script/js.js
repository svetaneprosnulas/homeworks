/*
При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". 
Данная кнопка должна являться единственным контентом в теле НTML документа, 
весь остальной контент должен быть создан и добавлен на страницу с помощью Јavascript.
При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. 
При нажатии на кнопку "Нарисовать" создать на странице 100кругов (10*10) случайного цвета. 
При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, 
то есть все остальные круги сдвигаются влево.
*/
window.onload = () => {

    const btn = document.getElementById('btn');
    let circles = [];
    let area = document.createElement('div');
    area.id = "area";
    document.body.append(area);

    for (i = 0; i < 100; i++) {
        let div = document.createElement('div');
        div.id = 'round';
        div.style.background = '#' + (Math.random().toString(16) + '000000').substring(2, 8).toUpperCase();
        area.appendChild(div);
        circles.push(div);

        div.onclick = () => {
            div.parentNode.removeChild(div); //удаляем элементы со сдвигом влево
        }
    }

    btn.onclick = () => {
        let a = +prompt('Введите диаметр круга') + 'px';
        for (j = 0; j < i; j++) {
            circles[j].style.width = circles[j].style.height = a;
            circles[j].style.visibility = 'visible';

        }
    }
}