/*                                       ЗАДАНИЕ 1
function Human(name, sex, age) { //функция-конструктор которая создает обьект Human
    this.name = name;
    this.sex = sex;
    this.age = age;
}
//заполняем инфой
Ross = new Human("Ross", "male", 28);
Joey = new Human("Joey", "male", 22);
Chandler = new Human("Chandler", "male", 36);

let arr = [Ross, Joey, Chandler]; //делаем массив

function sortByAge(arr) { //сортировка по возрастанию age
    arr.sort((a, b) => a.age > b.age ? 1 : -1);
}

sortByAge(arr); //вызываем в Scope->Script
*/





//                                  ЗАДАНИЕ 2
function Human(name, job, age) { //функция-конструктор которая создает обьект Human
    //добавляем свойства для конструктора Human
    this.name = name;
    this.job = job;
    this.age = age; 

    // добавляем метод для конструктора Human который будет представлять нам Human-ов)
    this.who = function who() {
        document.write('<br>My name is ' + this.name + ', i am ' + this.age + ' years old.');
        document.write(' I am ' + this.job + " And i am " + Human.nationality);
    }
}
Human.nationality = "ukrainian."; //  свойство для функции-конструктора 

//Теперь мы можем создавать объекты 

human1 = new Human('Matt', 'designer', 26);
//и обращаться к методу who созданного объекта 
human1.who();

human2 = new Human('Mary', 'developer', 25);
this.city = "Kyiv"; //добавляем свойство для одного из экземпляров 
human2.who() + document.write(" City of my birdth - " + this.city);
